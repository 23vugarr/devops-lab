## Folder structure

-------------------------------------------------
### Ansible
Please look at Gitlab variables
#
I have inventory in which i am storing gitlab.pem (it is also available in gitlab variables but for working on local i have put it in here)
#
I have 2 playbooks: db and backend
#
Inside playbooks i included roles
#
There are also seperate tasks
#
DB password is protected with vault. Password is test

### Application is deployed on aws
Backend is a simple application that adds tasks
#
I created dockerfile and it is built and pushed to ECR
#
Then ansible pulls image from ecr and deployes it on AWS EC2 machine