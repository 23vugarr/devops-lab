from pydantic import BaseModel

class Post(BaseModel):
    title: str
    content: str

class PostWithRef(BaseModel):
    title: str
    content: str
    referenceLinkUrl: str

class PostAndRef(BaseModel):
    title: str
    content: str
    referenceLinkUrl: str