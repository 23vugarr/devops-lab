from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session
from app.core.database import get_db
from app.schemas.post import Post, PostWithRef, PostAndRef
from sqlalchemy.sql import text

router = APIRouter()

@router.post("/post")
def addPost(post: Post, db: Session = Depends(get_db)):
    try:
        db.execute(
            text('INSERT INTO posts_table (title, content) VALUES (:title, :content)'),
            {"title": post.title, "content": post.content}
        )
        db.commit()
        return {"success": "added"}
    except Exception as e:
        print(e)
        db.rollback()
        return {"error": "Failed to add post"}
    

@router.post("/postwithref")
def addPostWithReference(post: PostWithRef, db: Session = Depends(get_db)):
    try:
        post = post.model_dump()
        db.execute(
            text('INSERT INTO posts_table_2 (title, content, reference_link_url) VALUES (:title, :content, :reference_link_url)'),
            {
                "title": post["title"],
                "content": post["content"],
                "reference_link_url": post["referenceLinkUrl"]
            }
        )
        db.commit()
        return {"success": "added"}
    except Exception as e:
        print(e)
        db.rollback()
        return {"error": "Failed to add post"}


@router.post("/postandref")
def addPostAndReference(post: PostAndRef, db: Session = Depends(get_db)):
    try:
        post = post.model_dump()
        db.execute(
            text('INSERT INTO reference_table_3 (link) VALUES (:link)'),
            {"link": post["referenceLinkUrl"]}
        )
        db.commit()

        latest_reference_id = db.execute(
            text('SELECT reference_id FROM reference_table_3 ORDER BY reference_id DESC LIMIT 1')
        ).scalar()

        db.execute(
            text('INSERT INTO posts_table_3 (title, content, reference_id) VALUES (:title, :content, :referenceID)'),
            {
                "title": post["title"],
                "content": post["content"],
                "referenceID": latest_reference_id
            }
        )
        db.commit()

        return {"success": "Post and reference added successfully"}
    except Exception as e:
        print(e)
        db.rollback()
        return {"error": "Failed to add post and reference"}
