from locust import HttpUser, task, between
import json
from faker import Faker

fake = Faker()

class LoadTestUser(HttpUser):

    @task(1)
    def add_post(self):
        post_data = {
            "title": fake.sentence(),
            "content": fake.paragraph()
        }
        headers = {'Content-Type': 'application/json'}
        response = self.client.post("/add/post", data=json.dumps(post_data), headers=headers)
        print(response.text)

    @task(2)
    def add_post_with_ref(self):
        post_data = {
            "title": fake.sentence(),
            "content": fake.paragraph(),
            "referenceLinkUrl": fake.url()
        }
        headers = {'Content-Type': 'application/json'}
        response = self.client.post("/add/postwithref", data=json.dumps(post_data), headers=headers)
        print(response.text)

    @task(3)
    def add_post_and_ref(self):
        post_data = {
            "title": fake.sentence(),
            "content": fake.paragraph(),
            "referenceLinkUrl": fake.url()
        }
        headers = {'Content-Type': 'application/json'}
        response = self.client.post("/add/postandref", data=json.dumps(post_data), headers=headers)
        print(response.text)
