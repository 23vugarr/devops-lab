from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from app.routers.post import router as PostRouter

def create_application():
    app = FastAPI()

    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_methods=["*"],
        allow_headers=["*"],
        allow_credentials=True
    )

    app.include_router(PostRouter, prefix="/add")

    return app

