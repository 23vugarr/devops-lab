from app.core.main import create_application
from fastapi.responses import RedirectResponse

app = create_application()

@app.get("/")
def read_root():
    return RedirectResponse(url='/docs')